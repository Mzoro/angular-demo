import {ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef} from '@angular/core';
import {AdHeroComponent} from './ad-hero/ad-hero.component';
import {AdStudentComponent} from './ad-student/ad-student.component';
import {Advertisement} from './advertisement';

/* tslint:disable */
@Directive({
  selector: '[appAdDirective]'
})
export class AdDirectiveDirective implements OnInit {
  @Input() data;

  constructor(public viewContainerRef: ViewContainerRef, private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {
    this.creatView();
  }

  creatView() {

    // viewContainerRef可以获取使用这个指令的标签对象，用它可以清除标签里的内容，也可以向其内部添加内容
    this.viewContainerRef.clear();
    const type = this.data.type;
    let c;
    // 创建一个组件需要它的构造方法，同时需要在app.module.ts的NgModule中加入
    // entryComponents: [AdStudentComponent, AdHeroComponent],
    if (type === 'hero') {
      c = AdHeroComponent;
    } else {
      c = AdStudentComponent;
    }

    // 创建这个组件
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(c);
    // 给这个组件中的data属性赋值
    const componentRef = this.viewContainerRef.createComponent(componentFactory);
    (<Advertisement>componentRef.instance).data = this.data.data;

  }
}
