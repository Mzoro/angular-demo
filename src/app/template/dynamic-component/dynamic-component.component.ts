import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-dynamic-component',
  templateUrl: './dynamic-component.component.html',
  styleUrls: ['./dynamic-component.component.css']
})
export class DynamicComponentComponent implements OnInit {

  data = [{
    type: 'hero',
    data: {id: 1, name: 'zoro'}
  }, {
    type: 'student',
    data: {weight: 22, name: 'pig'}
  }];

  constructor() {
  }

  ngOnInit() {
  }

}
