import {Component, OnInit} from '@angular/core';
import {Advertisement} from '../advertisement';
import {Hero} from '../../../hero';

@Component({
  selector: 'app-ad-hero',
  templateUrl: './ad-hero.component.html',
  styleUrls: ['./ad-hero.component.css']
})
export class AdHeroComponent implements OnInit, Advertisement {

  data: Hero;

  constructor() {
  }

  ngOnInit() {
  }

}
