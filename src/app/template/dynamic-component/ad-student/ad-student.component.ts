import {Component, OnInit} from '@angular/core';
import {Advertisement} from '../advertisement';
import {Student} from '../../../student';

@Component({
  selector: 'app-ad-student',
  templateUrl: './ad-student.component.html',
  styleUrls: ['./ad-student.component.css']
})
export class AdStudentComponent implements OnInit, Advertisement {

  data: Student;

  constructor() {
  }

  ngOnInit() {
  }

}
