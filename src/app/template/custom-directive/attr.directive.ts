import {Directive, ElementRef, HostListener, Input,} from '@angular/core';

@Directive({
  selector: '[myAppAttr]'
})
export class AttrDirective {

  @Input() colors: string[];
  i = 0;
  j = 0;

  constructor(private el: ElementRef) {
    console.error(el.nativeElement);
    el.nativeElement.style.height = '200px';
    el.nativeElement.style.width = '200px';
  }

  colorEvent() {
    if (this.colors) {
      if (this.i >= this.colors.length - 1) {
        this.i = 0;
      } else {
        this.i++;
      }

      this.el.nativeElement.style.backgroundColor = this.colors[this.i];
    } else {
      this.el.nativeElement.style.backgroundColor = '';
    }
  }

  @HostListener('mouseenter') mouseenter() {
    this.colorEvent();
  }

  @HostListener('mouseout') mouseouter() {
    this.colorEvent();
  }

  @HostListener('click') click() {
    if (this.j % 2 === 0) {
      this.el.nativeElement.style.fontWeight = 900;
    } else {
      this.el.nativeElement.style.fontWeight = 300;
    }
    this.j++;
  }

}
