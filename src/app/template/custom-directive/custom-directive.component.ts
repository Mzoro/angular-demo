import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-custom-directive',
  templateUrl: './custom-directive.component.html',
  styleUrls: ['./custom-directive.component.css']
})
export class CustomDirectiveComponent implements OnInit {
  colors = ['red', 'yellow', 'blue', '#3c3c3c'];

  constructor() {
  }

  ngOnInit() {
  }

}
