import {Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class StructuralDirective implements OnInit {


  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef) {
  }

  ngOnInit(): void {
  }

  @Input() set appUnless(loop: number) {
    this.viewContainer.clear();
    for (let i = 0; i < loop; i++) {
      console.log(this.templateRef);
      const native = this.templateRef.elementRef.nativeElement;
      console.log(native);
      const v = this.viewContainer.createEmbeddedView(this.templateRef);
      console.log(v);
      v.rootNodes[0].innerHTML = 'haha' + i;
    }
  }

}
