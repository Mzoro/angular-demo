import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-attribute',
  templateUrl: './attribute.component.html',
  styleUrls: ['./attribute.component.css']
})
export class AttributeComponent implements OnInit {

  describe = `DOM属性与HTML属性不是完成对应的,有些HTML元素的属性DOM元素是没有这,比如colspan/rowspan,这时如果要动态改变视图就不够了`;
  constructor() {
  }

  ngOnInit() {
  }

}
