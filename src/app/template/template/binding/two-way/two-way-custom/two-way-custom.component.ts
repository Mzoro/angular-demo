import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-two-way-custom',
  templateUrl: './two-way-custom.component.html',
  styleUrls: ['./two-way-custom.component.css']
})
export class TwoWayCustomComponent implements OnInit {

  @Input() size: number;
  @Output() sizeChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  inputEvent(e) {
    this.sizeChange.emit(e.target.value);
  }

}
