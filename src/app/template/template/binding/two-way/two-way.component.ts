import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-two-way',
  templateUrl: './two-way.component.html',
  styleUrls: ['./two-way.component.css']
})
export class TwoWayComponent implements OnInit {

  count = 0;
  count2 = 5;

  constructor() {
  }

  ngOnInit() {
  }

  countPlus(f): void {
    this.count += f;
  }

  count2Change(e): void {
    this.count2 = e.target.value;
  }

}
