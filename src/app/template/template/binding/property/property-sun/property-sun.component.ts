import {Component, Input, OnInit} from '@angular/core';
import {Student} from '../../../../../student';

@Component({
  selector: 'app-property-sun',
  templateUrl: './property-sun.component.html',
  styleUrls: ['./property-sun.component.css']
})
export class PropertySunComponent implements OnInit {
  @Input() student: Student;
  messageClass = '';

  constructor() {
  }

  ngOnInit() {
  }

  message(): string {
    if (!this.student.weight) {
      return '';
    }

    if (this.student.weight < 80) {
      this.messageClass = 'ok';
      return '标准体重';
    } else {
      this.messageClass = 'danger';
      return '超重了';
    }
  }
}
