import {Component, OnInit} from '@angular/core';
import {Student} from '../../../../student';

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  src = '#';
  src1 = 'https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg';
  src2 = 'https://ng.ant.design/assets/img/logo.svg';
  s = 0;
  student: Student = {name: ''};
  angularMessage = `
  在多数情况下，插值表达式是更方便的备选项。
  当要渲染的数据类型是字符串时，没有技术上的理由证明哪种形式更好。 你倾向于可读性，所以倾向于插值表达式.
  建议建立代码风格规则，选择一种形式， 这样，既遵循了规则，又能让手头的任务做起来更自然。但数据类型不是字符串时，就必须使用属性绑定了。
  `;

  constructor() {
  }

  ngOnInit() {
  }

  changeImg(src: number): void {
    if (src === 1) {
      this.src = this.src1;
    }
    if (src === 2) {
      this.src = this.src2;
    }

    this.s = src;
  }

}
