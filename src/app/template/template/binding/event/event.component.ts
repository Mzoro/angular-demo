import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  count = 0;

  name: string;
  weight: number;

  constructor() {
  }

  ngOnInit() {
  }

  countPlus(): void {
    this.count++;
  }

  eventHandler(s): void {
    console.log('收到数据：');
    console.log(s);
    this.name = s.name;
    this.weight = s.weight;
  }

}
