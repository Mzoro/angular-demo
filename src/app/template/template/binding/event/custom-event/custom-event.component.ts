import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Student} from '../../../../../student';

@Component({
  selector: 'app-custom-event',
  templateUrl: './custom-event.component.html',
  styleUrls: ['./custom-event.component.css']
})
export class CustomEventComponent implements OnInit {

  @Output() eventDemo = new EventEmitter<Student>();
  student: Student = {name: ''};

  constructor() {
  }

  ngOnInit() {
  }

  testEvent(): void {
    console.log('触发事件！');
    this.eventDemo.emit(this.student);
  }

}
