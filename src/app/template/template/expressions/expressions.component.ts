import {Component, OnInit} from '@angular/core';
import {Hero} from '../../../hero';

@Component({
  selector: 'app-expressions',
  templateUrl: './expressions.component.html',
  styleUrls: ['./expressions.component.css']
})
export class ExpressionsComponent implements OnInit {
  svg = 'https://img.alicdn.com/tfs/TB1NvvIwTtYBeNjy1XdXXXXyVXa-89-131.svg';
  disable = false;

  hero: Hero = {
    id: 2,
    name: 'sanji'
  };
  heroes: Hero[] = [{
    id: 3,
    name: 'joba'
  }, {
    id: 4,
    name: 'nami'
  }];

  constructor() {
  }

  ngOnInit() {
  }

  testMethod() {
    return 4;
  }

  testInput(value) {
    console.log(value);
  }
}
