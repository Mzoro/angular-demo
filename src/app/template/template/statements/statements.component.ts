import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-statements',
  templateUrl: './statements.component.html',
  styleUrls: ['./statements.component.css']
})
export class StatementsComponent implements OnInit {

  text: string;
  data: string[] = ['正在绑定事件的那个组件实例', '模板输入变量', '模板引用变量', '$event'];

  constructor() {
  }

  ngOnInit() {
  }

  nameChange(name) {
    this.text = 'My name is ' + name;
  }

}
