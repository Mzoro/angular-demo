import {Component, OnInit} from '@angular/core';
import {Hero} from '../../../hero';

@Component({
  selector: 'app-interpolation',
  templateUrl: './interpolation.component.html',
  styleUrls: ['./interpolation.component.css']
})
export class InterpolationComponent implements OnInit {

  hero: Hero = {
    id: 1,
    name: 'zoro'
  };

  svg = 'https://img.alicdn.com/tfs/TB1NvvIwTtYBeNjy1XdXXXXyVXa-89-131.svg';

  constructor() {
  }

  say() {
    return `I'm ${this.hero.name}, my id is ${this.hero.id}`;
  }

  ngOnInit() {
  }

}
