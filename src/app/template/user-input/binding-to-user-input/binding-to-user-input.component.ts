import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-binding-to-user-input',
  templateUrl: './binding-to-user-input.component.html',
  styleUrls: ['./binding-to-user-input.component.css']
})
export class BindingToUserInputComponent implements OnInit {
  keys: string;

  constructor() {
  }

  ngOnInit() {
  }

  keyEvent(v): void {
    this.keys = v;
  }

}
