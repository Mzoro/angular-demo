import {Pipe, PipeTransform} from '@angular/core';
import {Student} from '../../student';

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {

  transform(value: Student[], arg: number): Student[] {
    return value.filter((v => v.weight > arg));
  }

}
