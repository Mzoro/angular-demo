import {Component, OnInit} from '@angular/core';
import {Student} from '../../student';

@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styleUrls: ['./pipe.component.css']
})
export class PipeComponent implements OnInit {

  students: Student[] = [];
  addStu = {name: '名字'};
  flag = 100;

  constructor() {
  }

  ngOnInit() {
  }

  add() {
    // 这里不用push，因为没有改变 student变量的引用，所以pipe 不会检测到数据变更
    this.students = this.students.concat(this.addStu);
    this.addStu = new Student();
  }

}
