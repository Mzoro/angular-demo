import { Component, OnInit } from '@angular/core';
import {Student} from '../../../student';
import {CommunicationService} from '../communication.service';

@Component({
  selector: 'app-communication2',
  templateUrl: './communication2.component.html',
  styleUrls: ['./communication2.component.css']
})
export class Communication2Component implements OnInit {
  student = new Student();

  constructor(private commService: CommunicationService) {
  }

  ngOnInit() {
    this.commService.studentSubject.subscribe(next => {
      this.student = {name: next + ' communication-2'} as Student;
    });
  }

  changeName(name: String): void {
    this.commService.changeStudent(name);
  }

}
