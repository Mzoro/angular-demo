import {Component, OnInit} from '@angular/core';
import {interval, Observable, from} from 'rxjs';

@Component({
  selector: 'app-communication',
  templateUrl: './communication.component.html',
  styleUrls: ['./communication.component.css']
})
export class CommunicationComponent implements OnInit {

  start = 1;
  observable: Observable<number>;

  constructor() {
  }

  ngOnInit() {
  }

  startTest() {
    this.start = 2;
    this.observable = new Observable(subscriber => {
      subscriber.next(1);
      subscriber.next(2);
      subscriber.next(3);
      setTimeout(() => {
        subscriber.next(4);
        subscriber.complete();
      }, 1000);
    });
  }

  testRx() {
    this.observable.subscribe({
      next(x) {
        console.log('got value ' + x);
      },
      error(err) {
        console.error('something wrong occurred: ' + err);
      },
      complete() {
        console.log('done');
      }
    });
    const source = from([1, 2, 3]);
    const inter = interval(500);
  }
}
