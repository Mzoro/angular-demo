import {Injectable} from '@angular/core';
import {Student} from '../../student';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {
  student: Student;
  studentSubject = new Subject();

  constructor() {
  }

  changeStudent(name: String) {
    this.studentSubject.next(name);
  }
}
