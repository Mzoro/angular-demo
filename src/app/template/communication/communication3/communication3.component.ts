import {Component, OnInit} from '@angular/core';
import {CommunicationService} from '../communication.service';
import {Student} from '../../../student';

@Component({
  selector: 'app-communication3',
  templateUrl: './communication3.component.html',
  styleUrls: ['./communication3.component.css']
})
export class Communication3Component implements OnInit {

  student = new Student();

  constructor(private commService: CommunicationService) {
  }

  ngOnInit() {
    this.commService.studentSubject.subscribe(next => {
      console.log('sub');
      this.student = {name: next + ' communication-1'} as Student;
    });
  }

  changeName(name: String): void {
    this.commService.changeStudent(name);
  }

  changeName2() {
    this.commService.changeStudent(this.student.name);
  }

}
