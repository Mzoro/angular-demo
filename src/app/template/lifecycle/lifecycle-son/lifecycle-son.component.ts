import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck, Input,
  OnChanges,
  OnDestroy,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-lifecycle-son',
  templateUrl: './lifecycle-son.component.html',
  styleUrls: ['./lifecycle-son.component.css']
})
export class LifecycleSonComponent implements OnInit,
  OnChanges, DoCheck, AfterContentInit, AfterContentChecked,
  AfterViewInit, AfterViewChecked, OnDestroy {
  @Input() haha: string;

  constructor() {
  }

  changeHaHa() {
    this.haha = 'haha' + Math.random();
  }

  log(msg: string): void {
    console.log(msg);
    // console.log(this);
    // console.log('........................');
  }

  ngOnChanges(v) {
    console.log(v);
    this.log('ngOnChanges');
  }


  ngOnInit() {
    this.log('ngOnInit');
  }

  ngDoCheck() {
    this.log('ngDoCheck');
  }

  ngAfterContentInit() {
    this.log('ngAfterContentInit');
  }

  ngAfterContentChecked() {
    this.log('ngAfterContentChecked');
  }

  ngAfterViewInit() {
    this.log('ngAfterViewInit');
  }

  ngAfterViewChecked() {
    this.log('ngAfterViewChecked');
  }

  ngOnDestroy() {
    this.log('ngOnDestroy');
  }


}
