import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.css']
})
export class ReactiveFormsComponent implements OnInit {

  name = new FormControl('name');
  sex = new FormControl('2');
  heroNames = [
    new FormControl({name: 'zoro', value: true}),
    new FormControl({name: 'robin', value: true}),
    new FormControl({name: 'sanji', value: true})
  ];
  animals = [{name: '猴子', value: 1}, {name: '猪', value: 2}, {name: '猫', value: 3}];
  ani = new FormControl(this.animals[1]);

  // TODO checkbox 如何绑定 FormControl,
  // TODO 怕是绑定不了了,只能手写了
  constructor() {
  }

  ngOnInit() {
  }

  update() {
    this.name.setValue('name2');
    this.sex.setValue('1');
    this.ani.setValue(this.animals[2]);
  }
}
