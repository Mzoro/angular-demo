import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.css']
})
export class FormBuilderComponent implements OnInit {
  user = this.fb.group({
    name: ['小绿', Validators.required],
    age: [15, [Validators.min(20), Validators.max(300)]],
    address: this.fb.group({
      street: ['南二'],
      num: [222]
    }),
    alis: this.fb.array([this.fb.control('')])
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
  }

  get alis(): FormArray {
    return this.user.get('alis') as FormArray;
  }

}
