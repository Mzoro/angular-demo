import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';
import {Directive, Input} from '@angular/core';

@Directive({
  selector: '[appValidationName]',
  providers: [{provide: NG_VALIDATORS, useExisting: ValidationNameDirective, multi: true}]
})
export class ValidationNameDirective implements Validator {

  // 模板的 input标签上传入这个值 input中如果不写与 selector中一样的值，那么在使用这个指令时，还要另外写一个属性，所以方便点就写成一样的
  @Input('appValidationName') name: string;

  validate(control: AbstractControl): ValidationErrors | null {
    // 名字中不让有“红”字
    if (!control.value) {
      return null;
    }
    console.log('自定义验证器-模板用', this.name, control.value);
    return control.value.indexOf(this.name) >= 0 ? {hasErrorName: {name: this.name}} : null;
  }
}
