import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.css']
})
export class FormTemplateComponent implements OnInit {
  animals = [{id: 1, name: '猴子'}, {id: 2, name: '山羊'}, {id: 3, name: '猪'}];
  user = {name: '', sex: '男', rememberMe: true, animal: this.animals[1]};

  constructor() {
  }

  ngOnInit() {
  }

  submitEvent() {
    console.log(this.user);
  }

  test(d) {
    console.log(d);
  }

  // TODO 异步验证 Observable,http两种情况
}
