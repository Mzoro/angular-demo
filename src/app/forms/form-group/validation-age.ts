/**
 * 这是一个自定义的验证器的工厂方法
 */
import {AbstractControl, ValidatorFn} from '@angular/forms';

export function validationAge(min: number): ValidatorFn {

  return (control: AbstractControl): null | { [key: string]: any } => {
    return control.value > min ? null : {tooYoung: {ageNow: 'sdf'}};
    // 如果通过验证返回null
    // 如果没有通过，返回这个对象{tooYoung: {ageNow: 'sdf'}}，这个对象显示在FormControl的error属性中
  };

}
