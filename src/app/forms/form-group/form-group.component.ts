import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {validationAge} from './validation-age';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.css']
})
export class FormGroupComponent implements OnInit {

  // validationAge 是一个自定义验证方法
  user = new FormGroup({
    name: new FormControl('小红'),
    age: new FormControl('18', [validationAge(12), Validators.required]),
    address: new FormGroup({
      street: new FormControl('南'),
      num: new FormControl('399')
    })
  });

  constructor() {
  }

  ngOnInit() {
  }

  updateUser() {
    // 修补（Patching）模型值
    this.user.patchValue({
      address: {
        num: 100
      }
    });
  }

  updateUser2() {

    // 这里就会报错,setValue的参数要完全按照 user 的结构,但是patchValue就不用
    this.user.setValue({
      name: '小王'
    });
  }

  test(v) {
    console.log(v);
  }

  ageTest() {
    console.log(this.user.get('age'));
  }
}
