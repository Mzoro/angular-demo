import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

/**
 * 这里这个不是工厂方法，直接是一个方法，所以没有可以传参数的地方，但是也可以使用工厂方法，来创建一个ValidatorFn,比如 1.5这个系数就可以变为可变的参数
 * @param control control
 */
export const checkHeight: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const height = control.get('height').value;
  const weight = control.get('weight').value;
  return weight / height > 1.5 ? {'tooHeavy': {weight: weight, msg: 1.5}} : null;
};

export function checkHeight2(v: number): ValidatorFn {

  return (control: FormGroup): ValidationErrors | null => {

    const height = control.get('height').value;
    const other = control.get('other');
    const foot = other.get('foot').value;
    return height / foot > v ? {'too tall': {height: height, msg: v}} : null;
  };

}
