import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {checkHeight} from './height-check';
import {checkHeight2} from './height-check';

@Component({
  selector: 'app-form-cross-validation',
  templateUrl: './form-cross-validation.component.html',
  styleUrls: ['./form-cross-validation.component.css']
})
export class FormCrossValidationComponent implements OnInit {
  user = new FormGroup({
    height: new FormControl(''),
    weight: new FormControl(''),
    other: new FormGroup({
      foot: new FormControl()
    })
  }, {validators: checkHeight2(10)});

  constructor() {
  }

  ngOnInit() {
  }

}
