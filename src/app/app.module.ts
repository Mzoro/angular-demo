import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import {AppComponent} from './app.component';
import {NgZorroAntdModule, NZ_I18N, zh_CN} from 'ng-zorro-antd';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {registerLocaleData} from '@angular/common';
import zh from '@angular/common/locales/zh';
import {InterpolationComponent} from './template/template/interpolation/interpolation.component';
import {ExpressionsComponent} from './template/template/expressions/expressions.component';
import {StatementsComponent} from './template/template/statements/statements.component';
import {PropertyComponent} from './template/template/binding/property/property.component';
import {BindingComponent} from './template/template/binding/binding.component';
import {PropertySunComponent} from './template/template/binding/property/property-sun/property-sun.component';
import {AttributeComponent} from './template/template/binding/attribute/attribute.component';
import {EventComponent} from './template/template/binding/event/event.component';
import {CustomEventComponent} from './template/template/binding/event/custom-event/custom-event.component';
import {TwoWayComponent} from './template/template/binding/two-way/two-way.component';
import {TwoWayCustomComponent} from './template/template/binding/two-way/two-way-custom/two-way-custom.component';
import {TodoComponent} from './todo/todo.component';
import {ReferenceComponent} from './template/template/reference/reference.component';
import {BindingToUserInputComponent} from './template/user-input/binding-to-user-input/binding-to-user-input.component';
import {LifecycleComponent} from './template/lifecycle/lifecycle.component';
import {LifecycleSonComponent} from './template/lifecycle/lifecycle-son/lifecycle-son.component';
import {CommunicationComponent} from './template/communication/communication.component';
import {Communication1Component} from './template/communication/communication1/communication1.component';
import {Communication2Component} from './template/communication/communication2/communication2.component';
import {Communication3Component} from './template/communication/communication3/communication3.component';
import {DynamicComponentComponent} from './template/dynamic-component/dynamic-component.component';
import {AdDirectiveDirective} from './template/dynamic-component/ad-directive.directive';
import {AdHeroComponent} from './template/dynamic-component/ad-hero/ad-hero.component';
import {AdStudentComponent} from './template/dynamic-component/ad-student/ad-student.component';
import {AttrDirective} from './template/custom-directive/attr.directive';
import {CustomDirectiveComponent} from './template/custom-directive/custom-directive.component';
import {StructuralDirective} from './template/custom-directive/structural.directive';
import {PipeComponent} from './template/pipe/pipe.component';
import {NamePipe} from './template/pipe/name.pipe';
import {ReactiveFormsComponent} from './forms/reactive-forms/reactive-forms.component';
import {FormGroupComponent} from './forms/form-group/form-group.component';
import {FormBuilderComponent} from './forms/form-builder/form-builder.component';
import {FormTemplateComponent} from './forms/form-template/form-template.component';
import {TemplateVilidationComponent} from './forms/template-vilidation/template-vilidation.component';
import {ValidationNameDirective} from './forms/form-template/validation-name';
import {FormCrossValidationComponent} from './forms/form-cross-validation/form-cross-validation.component';

registerLocaleData(zh);

const declarations = [
  AppComponent,
  InterpolationComponent,
  ExpressionsComponent,
  StatementsComponent,
  PropertyComponent,
  BindingComponent,
  PropertySunComponent,
  AttributeComponent,
  EventComponent,
  CustomEventComponent,
  TwoWayComponent,
  TwoWayCustomComponent,
  TodoComponent,
  ReferenceComponent,
  BindingToUserInputComponent,
  LifecycleComponent,
  LifecycleSonComponent,
  CommunicationComponent,
  Communication1Component,
  Communication2Component,
  Communication3Component,
  DynamicComponentComponent,
  AdDirectiveDirective,
  AdHeroComponent,
  AdStudentComponent,
  CustomDirectiveComponent,
  AttrDirective,
  PipeComponent,
  StructuralDirective,
  NamePipe,
  ReactiveFormsComponent,
  FormGroupComponent,
  FormBuilderComponent,
  FormTemplateComponent,
  TemplateVilidationComponent,
  ValidationNameDirective,
  FormCrossValidationComponent

];

function f() {
  // 这说明不用所有的声明都写在这一个文件里，可以写在一个方法中，然后export这个方法，在这个文件import，但是这样lint总是报错，挺难看
  return declarations;
}

@NgModule({
  declarations: f(),
  imports: [
    BrowserModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule

  ],
  providers: [{provide: NZ_I18N, useValue: zh_CN}],
  entryComponents: [AdStudentComponent, AdHeroComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
